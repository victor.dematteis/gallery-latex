#!/bin/bash
rm main.pdf
/Library/TeX/texbin/pdflatex -quiet -synctex=1 -shell-escape -interaction=nonstopmode "main".tex
/Library/TeX/texbin/makeindex -s main.ist main.idx
/Library/TeX/texbin/pdflatex -quiet -synctex=1 -shell-escape -interaction=nonstopmode "main".tex

open main.pdf
