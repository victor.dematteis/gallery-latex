\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {chapter}{\numberline {1}Il progetto}{5}{chapter.1}%
\contentsline {section}{\numberline {1.1}Scenario}{5}{section.1.1}%
\contentsline {section}{\numberline {1.2}Schermate di esempio e struttura}{5}{section.1.2}%
\contentsline {section}{\numberline {1.3}Architettura di sistema}{8}{section.1.3}%
\contentsline {chapter}{\numberline {2}Ambiente di sviluppo}{9}{chapter.2}%
\contentsline {section}{\numberline {2.1}git}{9}{section.2.1}%
\contentsline {subsection}{\nonumberline Repository remoti}{9}{section*.2}%
\contentsline {subsection}{\nonumberline Inizializzazione e primo commit}{10}{section*.3}%
\contentsline {subsection}{\nonumberline Gli issues di GitLab}{11}{section*.4}%
\contentsline {subsection}{\nonumberline Operazioni successive}{12}{section*.5}%
\contentsline {section}{\numberline {2.2}IDE}{12}{section.2.2}%
\contentsline {subsection}{\nonumberline PHPstorm}{12}{section*.6}%
\contentsline {section}{\numberline {2.3}Virtualizzazione dell'ambiente}{12}{section.2.3}%
\contentsline {subsection}{\nonumberline vagrant}{12}{section*.7}%
\contentsline {subsection}{\nonumberline provisioning}{12}{section*.8}%
\contentsline {subsubsection}{\nonumberline Apache}{12}{section*.9}%
\contentsline {subsubsection}{\nonumberline PHP}{12}{section*.10}%
\contentsline {subsubsection}{\nonumberline mySQL / mariaDB}{12}{section*.11}%
\contentsline {section}{\numberline {2.4}Extra: debug di PHP con xdebug e PHPstorm}{12}{section.2.4}%
